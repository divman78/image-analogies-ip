import numpy as np
import cv2
import math
import colorsys
from annoy import AnnoyIndex


# A1 = cv2.imread('./images/newflower-src.jpg')
# A2 = cv2.imread('./images/newflower-emboss.jpg')
# B1 = cv2.imread('./images/toy-newshore-src.jpg')

# A1 = cv2.imread('./images/maple-blur-part4.jpg')
# A2 = cv2.imread('./images/maple-part4.jpg')
# B1 = cv2.imread('./images/maple-blur-part6.jpg')

# A1 = cv2.imread('./images/freud-crop-filt.jpg')
# A2 = cv2.imread('./images/freud-crop-src.jpg')
# B1 = cv2.imread('./images/shore-crop-src.jpg')

A1 = cv2.imread('./images/A1.jpg')
A2 = cv2.imread('./images/A2.jpg')
B1 = cv2.imread('./images/B1.jpg')

A1 = cv2.imread('./images/B11.jpg')
A2 = cv2.imread('./images/newflower-emboss.jpg')
B1 = cv2.imread('./images/toy-newshore-src.jpg')

# A1 = cv2.imread('./images/rhone-src.jpg')
# A2 = cv2.imread('./images/rhone.jpg')
# B1 = cv2.imread('./images/boats.jpg')

A1 = cv2.imread('./images/blurA1s.jpg')
A2 = cv2.imread('./images/blurA2s.jpg')
B1 = cv2.imread('./images/blurB1s.jpg')

# A1 = cv2.imread('./images/blurA1s.jpg')
# A2 = cv2.imread('./images/blurA2s.jpg')
# B1 = cv2.imread('./images/B1.jpg')

# A1 = cv2.imread('./images/oxbow-mask.jpg')
# A2 = cv2.imread('./images/oxbow.jpg')
# B1 = cv2.imread('./images/oxbow-newmask.jpg')


# A1 = cv2.imread('./images/rug-big-blur-part3.jpg')
# A2 = cv2.imread('./images/rug-big-sharp-part3.jpg')
# B1 = cv2.imread('./images/rug-big-blur.jpg')

# A1 = cv2.imread('./images/rhone-src.jpg')
# A2 = cv2.imread('./images/rhone.jpg')
# B1 = cv2.imread('./images/newflower-src.jpg')

# A1 = cv2.imread('./images/toy-newshore-src.jpg')
# A2 = cv2.imread('./images/toy-newshore-blur.jpg')
# B1 = cv2.imread('./images/rhone-src.jpg')

# A1 = colorsys.rgb_to_yiq(A1)
# A2 = colorsys.rgb_to_yiq(A2)
# B1 = colorsys.rgb_to_yiq(B1)



# for i in range(A1.shape[0]):
# 	for j in range(A1.shape[1]):
# 		A1[i][j] = colorsys.rgb_to_yiq(A1[i][j][0]/255.0, A1[i][j][1]/255.0, A1[i][j][2]/255.0)

# for i in range(A2.shape[0]):
# 	for j in range(A2.shape[1]):
# 		A2[i][j] = colorsys.rgb_to_yiq(A2[i][j][0]/255.0, A2[i][j][1]/255.0, A2[i][j][2]/255.0)

# for i in range(B1.shape[0]):
# 	for j in range(B1.shape[1]):
# 		B1[i][j] = colorsys.rgb_to_yiq(B1[i][j][0]/255.0, B1[i][j][1]/255.0, B1[i][j][2]/255.0)



BIG = 5
SMALL = 3
NUM_FEATURES = 3
NNF = BIG * BIG * 3
nnf = SMALL * SMALL * NUM_FEATURES
KAPPA = 0.1 #higher the favours ann


def fspecial_gauss(size, sigma=1):
    """Function to mimic the 'fspecial' gaussian MATLAB function
    """
    x, y = np.mgrid[-size//2 + 1:size//2 + 1, -size//2 + 1:size//2 + 1]
    g = np.exp(-((x**2 + y**2)/(2.0*sigma**2)))
    g = g/g.sum()
    gstar = np.zeros((size,size, NUM_FEATURES))
    for i in range(size):
    	for j in range(size):
    		temp = g[i][j]
    		gstar[i][j] = [temp, temp, temp]

    return gstar

#for normalization of finer and coarser level matrix
G_BIG = fspecial_gauss(5)
# print(G_BIG)
G_SMALL = fspecial_gauss(3)

#for adding border to four side of image X
def extend_Image(X, bordersize):
	row, col= X.shape[:2]
	bottom= X[row-2:row, 0:col]
	mean= int(cv2.mean(bottom)[0])
	bordersize = int(bordersize)
	#print((mean, X, bordersize))
	border=cv2.copyMakeBorder(X, top=bordersize, bottom=bordersize, left=bordersize, right=bordersize, borderType= cv2.BORDER_CONSTANT, value=[mean,mean,mean])
	return border

#for converting rgb features to yiq
def toyiq(X_features):
	# print(X_features)
	if(isinstance(X_features[0], list) == False): #edge case 
		return X_features
	for i in range(0,len(X_features)):
		for j in range(0, len(X_features[0])):
			X_features[i][j] = colorsys.rgb_to_yiq(X_features[i][j][0]/255.0, X_features[i][j][1]/255.0, X_features[i][j][2]/255.0) 
	return X_features



#converting array of size smaller than limit-by-limit to limit-by-limit by appending rows and cols of zeroes
def convarray(arr, limit):
	temp = np.zeros((limit, limit, NUM_FEATURES))
	print(arr.shape)
	(rows, cols,_) = arr.shape
	temp[0:rows, 0:cols, 0:NUM_FEATURES] = arr
	temp = np.array(temp)
	return temp




def concat_feature(X1_pyramid, X2_pyramid, l, i, j, L):
	F = np.zeros(((nnf*2)+(NNF*2)), np.float) #2*nnf for coarse features and 2*NNF for fine features of (i, j) 

	#for level l
	X1_fine = X1_pyramid[l]
	X2_fine = X2_pyramid[l]
	X1_fine_size = X1_fine.shape
	X2_fine_size = X2_fine.shape
           
	X1_fine_features = X1_fine[max(0,int(i-BIG/2)): min(X1_fine_size[0], int(i+BIG/2)), max(0,int(j-BIG/2)): min(X1_fine_size[1], int(j+BIG/2))]
	#X1_fine_features = toyiq(X1_fine_features)
	X1_fine_features = convarray(X1_fine_features, BIG)
	X1_fine_features = X1_fine_features*G_BIG  #normalize
	X1_fine_features = X1_fine_features.flatten()
	F[0:min(len(X1_fine_features), NNF)] = X1_fine_features

	X2_fine_features = X2_fine[max(0,int(i-BIG/2)): min(X2_fine_size[0], int(i+BIG/2)), max(0,int(j-BIG/2)): min(X2_fine_size[1], int(j+BIG/2))]
	#X2_fine_features = toyiq(X2_fine_features)
	X2_fine_features = convarray(X2_fine_features, BIG)
	X2_fine_features = X2_fine_features*G_BIG #normalize
	X2_fine_features = X2_fine_features.flatten()
	F[NNF: min(NNF + len(X2_fine_features), 2*NNF)] = X2_fine_features

    #for coarsest level
	if(l+1 >= L):
		return F 


    #for level l+1
	X1_coarse = X1_pyramid[l+1]
	X2_coarse = X2_pyramid[l+1]
	X1_coarse_size = X1_coarse.shape
	X2_coarse_size = X2_coarse.shape

	X1_coarse_features = X1_coarse[max(0,int(i/2-SMALL/2)): min(X1_coarse_size[0], int(i/2+SMALL/2)), max(0,int(j/2-SMALL/2)): min(X1_coarse_size[1], int(j/2+SMALL/2))]
	#X1_coarse_features = toyiq(X1_coarse_features)
	X1_coarse_features = convarray(X1_coarse_features, SMALL)
	X1_coarse_features = X1_coarse_features*G_SMALL #normalize
	X1_coarse_features = X1_coarse_features.flatten()
	F[2*NNF :min(2*NNF+len(X1_coarse_features),2*NNF+ nnf)] = X1_coarse_features

	X2_coarse_features = X2_coarse[max(0,int(i/2-SMALL/2)): min(X2_coarse_size[0], int(i/2+SMALL/2)), max(0,int(j/2-SMALL/2)): min(X2_coarse_size[1], int(j/2+SMALL/2))]
	#X2_coarse_features = toyiq(X2_coarse_features)
	X2_coarse_features = convarray(X2_coarse_features, SMALL)
	X2_coarse_features = X2_coarse_features*G_SMALL #normalize
	X2_coarse_features = X2_coarse_features.flatten()
	F[2*NNF+ nnf: min(2*NNF+ nnf + len(X2_coarse_features),2*NNF + 2*nnf)] = X2_coarse_features

	return F


def best_coherence_match(A_features, B_features, A1_pyramid, B1_pyramid, A2_pyramid, B2_pyramid, s_pyramid, l, L , i, j):
	(A_h, Aw, _) = A1_pyramid[l].shape
	border_big = int(math.floor(BIG/2))

	F_q = concat_feature(B1_pyramid, B2_pyramid, l, i ,j ,L)

	min_dist = float("inf")

	r_star_i = -1
	r_star_j = -1

	done = False
	(h, w, _) = B1_pyramid[l].shape

	#find the best coherent (ii,jj) of all synthesized neighbors of (i,j)
	for ii in range(max(0,i-border_big),min(h, i + border_big)):
		for jj in range(max(0,j-border_big),min(w ,j + border_big)):
			if ii == i and jj == j: #no pixel after(i,j) is synthesized, so we are done for (i,j)
				done = True
				break

			s_i = s_pyramid[l][ii][jj][0]
			s_j = s_pyramid[l][ii][jj][1]

			F_sr_i = s_i + (i-ii)
			F_sr_j = s_j + (j-jj)

			if (F_sr_i >= A_h or F_sr_i < 0 or F_sr_j >= Aw or F_sr_j < 0):
				continue

			F_sr = concat_feature(A1_pyramid, A2_pyramid, l, F_sr_i, F_sr_j, L)

			F_sr = np.array(F_sr)
			F_q= np.array(F_q)

			#print((F_sr), len(F_q));

			dist = sum(np.power(F_sr-F_q, 2))

			if(dist < min_dist):
				min_dist = dist
				r_star_i = ii
				r_star_j = jj
				best_coh_i = F_sr_i
				best_coh_j = F_sr_j 

		if(done):
			break
	if(r_star_i == -1 or r_star_j == -1):
		best_coh_i = -1
		best_coh_j = -1


	return (best_coh_i, best_coh_j)


def best_approximate_match(A_features, B_features, A1_pyramid, B1_pyramid,  A2_pyramid, B2_pyramid, l, L , i, j):


	Al_features = A_features[l]
	#row length of l th level pyramid
	h = len(Al_features)
	w = len(Al_features[0])

	#each feature length
	f = len(Al_features[0][0])

	query = B_features[l][i][j]


	#using annoy for ann
	u = AnnoyIndex(f, metric='manhattan')

	#loading the saved file for l th level in create_image_analogies function
	u.load('test.ann' + str(l)) # super fast, will just mmap the file
	ind = u.get_nns_by_vector(query, 1, search_k = 1000, include_distances=False)[0] #index of first ann to query 

	#one dimensional index to two dimensional index
	best_app_i = int(ind / w)
	best_app_j = ind - best_app_i*w
	print((ind,best_app_i*w))

	return (best_app_i, best_app_j)



def best_match(A1_pyramid, A2_pyramid, B1_pyramid, B2_pyramid, s_pyramid, A_features, B_features, l, L, i, j):

	(best_app_i, best_app_j) = best_approximate_match(A_features, B_features, A1_pyramid, B1_pyramid,  A2_pyramid, B2_pyramid, l, L , i, j)
	#return (best_app_i, best_app_j) 
	(best_coh_i, best_coh_j) = best_coherence_match(A_features, B_features, A1_pyramid, B1_pyramid, A2_pyramid, B2_pyramid, s_pyramid, l, L , i, j)
	best_i = -1
	best_j = -1
	if(best_coh_i == -1 or best_coh_j == -1):	
		(best_i, best_j) = (best_app_i, best_app_j)

	else:

		F_p_app = concat_feature(A1_pyramid, A2_pyramid, l, best_app_i, best_app_j, L)
		F_p_coh = concat_feature(A1_pyramid, A2_pyramid, l, best_coh_i, best_coh_j, L)

		F_q = concat_feature(B1_pyramid, B2_pyramid, l, i, j, L)
		d_app = sum(np.power((F_p_app - F_q),2));
		d_coh = sum(np.power((F_p_coh- F_q),2));


		if(d_coh <= d_app * (1 + (2^(l-L))*KAPPA)):
			best_i = best_coh_i
			best_j = best_coh_j

		else:
			
			best_i = best_app_i
			best_j = best_app_j

	return (best_i, best_j)








def create_image_analogies(A1, A2, B1):
	A1_pyramid = [A1]
	A2_pyramid = [A2]
	B1_pyramid = [B1]
	(hb, wb, _) = B1.shape
	s = np.zeros((hb, wb, 2), np.uint8)
	B2 = np.zeros((hb, wb, 3), np.uint8)
	B2_pyramid = [B2]
	s_pyramid = [s]
	(ha, wa, _) = A1.shape

	while(ha >= 50 and wa >= 50): #until image down to 50 by 50
		A1 = cv2.pyrDown(A1)
		A2 = cv2.pyrDown(A2)
		B1 = cv2.pyrDown(B1)
		B2 = cv2.pyrDown(B2)

		(hb, wb, _) = B1.shape
		(ha, wa, _) = A1.shape

		s = np.zeros((hb, wb, 2), np.uint8)

		A1_pyramid.append(A1)
		A2_pyramid.append(A2)
		B1_pyramid.append(B1)
		B2_pyramid.append(B2)
		s_pyramid.append(s)



	L = len(A1_pyramid)

	print("levels = "+ str(L))


    #extended pyramid for extracting features of edge pixels easily  
	A1_extended_pyramid = []
	B1_extended_pyramid = []

	for l in range(L):
		A1_extended_pyramid.append(extend_Image(A1_pyramid[l], BIG/2))
		B1_extended_pyramid.append(extend_Image(B1_pyramid[l], BIG/2))
		#cv2.imwrite("./images/BBDSDS" + str(l)+".jpg",extend_Image(B1_pyramid[l], BIG/2))




	#features for ann
	A_features = [None]*L
	B_features = [None]*L



	for l in range(L):
		(ha, wa, _) = A1_pyramid[l].shape
		Al_features = np.zeros((ha, wa, NNF), np.uint8)
		for i in range(ha):
			for j in range(wa):
				#Al_features[i][j] = A1_extended_pyramid[l][i : i + BIG, j : j + BIG].flatten()
				pp = 0
				for xx in range(i,i+BIG):
					for yy in range(j, j+BIG):
						Al_features[i][j][pp]= A1_extended_pyramid[l][xx][yy][0]
						Al_features[i][j][pp+1]= A1_extended_pyramid[l][xx][yy][1]
						Al_features[i][j][pp+2]= A1_extended_pyramid[l][xx][yy][2]
						pp = pp+3

		A_features[l] = Al_features


	for l in range(L):
		(hb, wb, _) = B1_pyramid[l].shape
		Bl_features = np.zeros((hb, wb, NNF), np.uint8)
		for i in range(hb):
			for j in range(wb):
				#Bl_features[i][j] = B1_extended_pyramid[l][i : i + BIG, j : j + BIG].flatten()
				pp = 0
				for xx in range(i,i+BIG):
					for yy in range(j, j+BIG):
						Bl_features[i][j][pp]= B1_extended_pyramid[l][xx][yy][0]
						Bl_features[i][j][pp+1]= B1_extended_pyramid[l][xx][yy][1]
						Bl_features[i][j][pp+2]= B1_extended_pyramid[l][xx][yy][2]
						pp=pp+3

		B_features[l] = Bl_features

	#from coarsest to finest level
	for l in range(L-1, -1, -1):

		#load l th level into ann model and save it to file
		Al_features = A_features[l]
		h = len(Al_features)
		f = len(Al_features[0][0])
		t = AnnoyIndex(f, metric='manhattan')  # Length of item vector that will be indexed
		for ii in range(h):
			for jj in range(len(Al_features[0])):
				v = Al_features[ii][jj]
				t.add_item(ii*len(Al_features[0])+jj, v)

		t.build(10) # 10 trees
		t.save('test.ann' + str(l))


		B2_l = B2_pyramid[l]
		(height, width, __) = B2_l.shape
		for i in range(0, height):
			for j in range(0, width):
				(best_i, best_j) = best_match(A1_pyramid, A2_pyramid, B1_pyramid, B2_pyramid, s_pyramid, A_features, B_features, l, L, i, j)
				# if(i == 2):
				best_i = int(best_i)
				best_j = int(best_j)
				# 	print((i,j,l))
				print((best_i, best_j, l))

				#take luminance factor of A[best_i][best_j]
				(AY, _, _) = colorsys.rgb_to_yiq(A2_pyramid[l][best_i][best_j][0]/255.0, A2_pyramid[l][best_i][best_j][1]/255.0, A2_pyramid[l][best_i][best_j][2]/255.0)

				(_,BI, BQ) = colorsys.rgb_to_yiq(B1_pyramid[l][i][j][0]/255.0, B1_pyramid[l][i][j][1]/255.0, B1_pyramid[l][i][j][2]/255.0)
				#print("A2" + str((A2_pyramid[l][best_i][best_j][0], A2_pyramid[l][best_i][best_j][1], A2_pyramid[l][best_i][best_j][2])))
				#print("B1" + str((B1_pyramid[l][i][j][0], B1_pyramid[l][i][j][1], B1_pyramid[l][i][j][2])))

				
				(r,g,b) = colorsys.yiq_to_rgb(AY, BI, BQ)
				#print("norm" + str((r,g,b)))
				r = r*255
				g = g*255
				b = b*255
				#print("org" + str((r,g,b)))
				#uncomment below line for embossing, mask filters etc.
				#(r,g,b) = (A2_pyramid[l][best_i][best_j][0], A2_pyramid[l][best_i][best_j][1], A2_pyramid[l][best_i][best_j][2])


				B2_pyramid[l][i][j][0] = r
				B2_pyramid[l][i][j][1] = g
				B2_pyramid[l][i][j][2] = b


				s_pyramid[l][i][j][0] = best_i
				s_pyramid[l][i][j][1] = best_j

	return B2_pyramid[0]





B2 = create_image_analogies(A1, A2, B1)


cv2.imwrite("./images/"+ "B2.jpg", B2)

# cv2.imshow('image', B2)
# cv2.waitKey(0)
#combinatrics_dp
