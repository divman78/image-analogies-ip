import cv2
import numpy as np,sys

A = cv2.imread('./images2/A.jpg')

A = cv2.resize(A, (256,256))
cv2.imwrite('./images2/X.jpg', A)


#Gaussian pyramid
G = A.copy()
gpA = [G]
for i in xrange(6):
    G = cv2.pyrDown(G)
    gpA.append(G)


#Laplacian Pyramid 
lpA = [gpA[5]]
for i in xrange(5,0,-1):
    GE = cv2.pyrUp(gpA[i])
    L = cv2.subtract(gpA[i-1],GE)
    lpA.append(L)

lpA.reverse()



#reconstruct original image
org = gpA[5]
#coarser to finer 5 --> 0
for i in xrange(5,0,-1):
    #decompress ith image
    ls = cv2.pyrUp(gpA[i])
    #add decompressed image to laplacian of finer level to i, i.e, i-1 
    org = cv2.add(ls, lpA[i-1])





cv2.imwrite('./images2/XX.jpg', org)
